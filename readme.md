## Dependencies

- Python: [lxml cssselect](https://lxml.de/cssselect.html)
- jotdown: `cargo install --git https://github.com/hellux/jotdown jotdown`
- [Tup](https://gittup.org/tup/)
