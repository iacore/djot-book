import sys, warnings
from os import path
from lxml.etree import HTML, tostring

template = open("template.html").read()
input = sys.stdin.read()
rendered = template.replace("{body}", input)
h = HTML(rendered)
for el in h.cssselect('a'):
    if 'href' not in el.attrib:
        warnings.warn('Empty <a> href')
        continue
    href = el.attrib['href']
    if '//' in href: continue
    # todo: consider relative path
    # right now only content/*.dj is supported
    p = path.join('content', href)
    if path.isfile(p):
        el.attrib['href'] = path.splitext(href)[0] +".html"
        continue
    p = path.join('content', href+".dj")
    if path.isfile(p):
        el.attrib['href'] = href+".html"
        continue

# todo: check links here
sys.stdout.write('<!DOCTYPE html>')
sys.stdout.write(tostring(h).decode())
